/*
Copyright (C) 2014 Nolan Baker

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions 
of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
DEALINGS IN THE SOFTWARE.
*/

using UnityEngine;
using System.Collections;
using Paraphernalia.Extensions;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(CircleCollider2D))]
public class CharacterMotor2D : MonoBehaviour {
 
	public float maxSpeedChange = 1.0f;
	public float jumpHeight = 1.0f;
	public float airMovement = 0.1f;
	public float jumpCancelRate = 0.5f;
	public bool inControl = true;
	public LayerMask environmentLayers = -1;
	
	[HideInInspector] public bool shouldJump = false;
	[HideInInspector] public bool cancelJump = false;
	[HideInInspector] public Vector2 targetVelocity = Vector2.zero;	
	private bool jumpCanceled = false;

	private int collisionCount = 0;
	private Vector2 _contactNorm = Vector2.zero;
	public Vector2 contactNorm {
		get { return _contactNorm; }
	}

	[Range(0, 90)] public float maxIncline = 30;
	public bool isGrounded {
		get { 
			int start = Mathf.RoundToInt((float)raycastCount * (270 - maxIncline) / 360);
			int end = Mathf.RoundToInt((float)raycastCount * (270 + maxIncline) / 360);
			for (int i = start; i < end; i++) {
				if (sweepResults[i % raycastCount].collider != null) {
					return true;
				}
			}
			return false;
		}
	}
	
	public bool inAir {
		get { return !isGrounded; }
	}

	public CircleCollider2D circleCollider {
		get { return (CircleCollider2D)collider2D; }
	}

	public float sweepDistance = 1;
	public int raycastCount = 8;
	public int stepsPerFixedUpdate = 2;
	private int raycastSweepIndex = 0;
	private RaycastHit2D[] _sweepResults;
	public RaycastHit2D[] sweepResults {
		get {
			if (_sweepResults == null || _sweepResults.Length != raycastCount) {
				_sweepResults = new RaycastHit2D[raycastCount];
			}
			return _sweepResults;
		}
	}

	public bool IsTouchingInDirection (Vector2 direction) {
		float frac = 0.5f + Mathf.Atan2(direction.y, direction.x) / (Mathf.PI * 2f);
		int index = Mathf.RoundToInt(frac * raycastCount) % raycastCount;
		return (sweepResults[index].collider != null);
	}

	void  UpdateSweep (int steps) {	
		for (int i = 0; i < steps; i++) {	
			float ang = 2f * Mathf.PI * (float)raycastSweepIndex / (float)raycastCount;
			Vector2 dir = new Vector2(Mathf.Cos(ang), Mathf.Sin(ang));
			Vector2 start = circleCollider.center + dir * circleCollider.radius;
			start += (Vector2)transform.position;
			sweepResults[raycastSweepIndex] = Physics2D.Linecast(
				start, 
				start + dir * sweepDistance, 
				environmentLayers
			);
			raycastSweepIndex = (raycastSweepIndex + 1) % raycastCount;
		}
	} 
 
	float CalculateJumpVerticalSpeed () {
		return Mathf.Sqrt(2 * jumpHeight * -Physics2D.gravity.y);
	}

	void UpdateForces () {
		Vector2 velocity = rigidbody2D.velocity;
		Vector2 velocityChange = (targetVelocity - velocity);
		velocityChange = velocityChange.normalized * Mathf.Clamp(velocityChange.magnitude, -maxSpeedChange, maxSpeedChange);
	 	velocityChange.y = 0;
			
		if (inAir) {
			_contactNorm = Vector2.up;
			velocityChange = velocityChange * airMovement;
			if (cancelJump && !jumpCanceled && rigidbody2D.velocity.y > 0) {
				jumpCanceled = true;
				velocityChange.y = -rigidbody2D.velocity.y * jumpCancelRate;
			}
		}
		else if (shouldJump) {
			rigidbody2D.velocity = new Vector2(velocity.x, CalculateJumpVerticalSpeed());
		}
		
		rigidbody2D.AddForce(velocityChange, ForceMode.VelocityChange);
	}
	
	void FixedUpdate () {
		UpdateSweep(stepsPerFixedUpdate);
		if (inControl) UpdateForces();
	}

	void OnCollisionEnter2D (Collision2D collision) {
		collisionCount++;
		// Debug.Log("Enter " + collisionCount);
		_contactNorm = Vector2.zero;
		for (int i = 0; i < collision.contacts.Length; i++) {
			_contactNorm += collision.contacts[i].normal;
		}
		_contactNorm /= (float)collision.contacts.Length;
		_contactNorm.Normalize();
	}

	void OnCollisionExit2D (Collision2D collision) {
		collisionCount--;
		// Debug.Log("Exit " + collisionCount);
	}

	void OnDrawGizmos () {
		Vector3 start = transform.position + (Vector3)circleCollider.center;
		Vector3 end = start + (Vector3)contactNorm * 2;
		Gizmos.DrawLine(start, end);
	}
}