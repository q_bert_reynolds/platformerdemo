This is a simple platformer demo built using Paraphernalia and PrettyPoly.

After cloning, you'll need to init and update those submodules:
	git submodule init
	git submodule update