﻿using UnityEngine;
using System.Collections;

public class ABugsLife : MonoBehaviour {

	public SpriteRenderer spriteRenderer;
	public Sprite deadSprite;
	public Sprite normalSprite;
	public Sprite confusedSprite;

	public float health = 1;

	private Color startColor;
	private Vector3 startPosition;

	private PlatformerController2D _controller;
	public PlatformerController2D controller {
		get {
			if (_controller == null) {
				_controller = GetComponent<PlatformerController2D>(); 
			}
			return _controller;
		}
	}

	void Start () {
		startColor = spriteRenderer.color;
		startPosition = transform.position;
	}

	void OnEnable () {
		PrettyPoly.PrettyPoly.onCollision2D += PrettyPolyCollision;
	}

	void OnDisable () {
		PrettyPoly.PrettyPoly.onCollision2D -= PrettyPolyCollision;
	}

	void PrettyPolyCollision (Collision2D collision, PrettyPoly.PrettyPolyLayer layer) {
		if (layer.name == "Spikes") {
			TakeDamage(1);
		}
	}

	void TakeDamage (float damage) {
		health -= damage;
		controller.enabled = false;
		controller.motor.enabled = false;

		if (health <= 0) {
			StartCoroutine("DeathCoroutine");
		}
		else {
			StopCoroutine("TakeDamageCoroutine");
			StartCoroutine("TakeDamageCoroutine");
		}
	}

	IEnumerator TakeDamageCoroutine () {
		spriteRenderer.color = Color.red;
		spriteRenderer.sprite = confusedSprite;
		yield return new WaitForSeconds(0.5f);
		controller.enabled = true;
		controller.motor.enabled = true;
		spriteRenderer.color = startColor;
		spriteRenderer.sprite = normalSprite;
	}

	IEnumerator DeathCoroutine () {
		spriteRenderer.color = Color.blue;
		spriteRenderer.sprite = deadSprite;
		yield return new WaitForSeconds(2);
		controller.enabled = true;
		controller.motor.enabled = true;
		transform.position = startPosition;
		spriteRenderer.color = startColor;
		spriteRenderer.sprite = normalSprite;
	}
}
